#!/bin/sh
set -e

mkdir build
cd build
cmake .. \
  -DMOJOSHADER_VERSION=1 \
  -DMOJOSHADER_CHANGESET=1
make test
cd ..
rm -rf build
